# mlopR-gitdvccml

This repository presents a ML pipeline based on GIT, DVC and CML. The pipeline is constructed along distinct data processing stages from data intake via cleaning, preparing and featurizing to model training, scoring and monitoring. GIT, DVC, and CML are all statistical computing language agnostic, such that R, Python, SQL and other languages can be used to implement the data processing stages.

This repository uses a Gitlab CI/CD pipeline, but could also be implementied in .e.g Github actions.

## Installation

Installation of the git client is not covered here. VS-Code with the Git, Python, R, DVC and Remote Development plugins has been used for most of the work.

### dvc

Self contained package via snapd

```
sudo yum makecache
sudo yum install snapd
sudo systemctl enable --now snapd.socket
sudo ln -s /var/lib/snapd/snap /snap

sudo snap install dvc --classic
PATH=/var/lib/snapd/snap/bin:$PATH

dvc -V
2.42.0
```

## Configuration

### DVC

Data directory for current project

```
mkdir -p ./dvcRemoteLocal/gitdvccml
```

Initialize

```
dvc init

# Use local disk as remote storage (may eventually be located on e.g. S3, COS or other cloud service)
dvc remote add --verbose --project --default gitdvccml  ../dvcRemoteLocal/gitdvccml
# Setting 'gitdvccml' as a default remote.
# 2023-01-20 19:10:21,420 DEBUG: Writing '/home/at001335/DataEngineering/mlopR-gitdvccml/.dvc/config'.
# 2023-01-20 19:10:21,423 DEBUG: Analytics is enabled.
# 2023-01-20 19:10:21,450 DEBUG: Trying to spawn '['daemon', '-q', 'analytics', '/tmp/tmp437zkoz6']'
# 2023-01-20 19:10:21,452 DEBUG: Spawned '['daemon', '-q', 'analytics', '/tmp/tmp437zkoz6']'

git add .dvc/.gitignore
git add .dvc/config
git commit -m "dvc init"
```

## Data Version Control

```
# Add data to dvc and commit references to git
dvc add ./data/remote/creditcard.csv.gz
git add data/remote/creditcard.csv.gz.dvc data/remote/.gitignore
git commit -m "Add data to dvc"

# Push data from local cache to remote storage
dvc push data/remote/creditcard.csv.gz

# Remove data file and restore from remote storage
rm ./data/remote/creditcard.csv.gz
dvc checkout data/remote/creditcard.csv.gz.dvc 

# Adding data
# dvc add ./data/remote/creditcard2.csv.gz
# git add data/remote/creditcard2.csv.gz.dvc data/remote/.gitignore
# git commit -m "Add data2 to dvc"
# dvc push

# Switching data version (provided that data have been changed on the branch)
# git checkout <branch>
# dvc checkout
```

## DVC Pipeline

A DVC pipeline consists of several related stages with scripts, input, output, parameters, etc and their dependencies

### Create stage

```
dvc stage add   -n ingest \
                -p ingest.param \
                -d scripts/ingest/ingest.R -d data/remote/creditcard.csv.gz \
                -o data/ingested \
                Rscript scripts/ingest/ingest.R data/remote/creditcard.csv.gz

# creates a dvc.yaml:
stages:
  ingest:
    cmd: Rscript scripts/ingest/ingest.R data/remote/creditcard.csv.gz
    deps:
    - data/remote/creditcard.csv.gz
    - scripts/ingest/ingest.R
    params:
    - ingest.param
    outs:
    - data/ingested

# which requires a params.yaml
ingest:
  param: 0
```

### Execute stage

```
dvc repro

# Add changes to git 
git add dvc.lock

# Push changes to remote
dvc push
```

More stages can be added by 'dvc add' or by editing the dvc.yaml file

### Commit relevant files to git, run pipeline and commit results to git
```
git add dvc.lock dvc.yaml params.yaml .gitignore clean.R featurize.R ingest.R 
dvc repro
git add data/.gitignore dvc.lock
dvc push
```

## Start from scratch with Pipeline

```
# Initialize
dvc init
dvc remote add --verbose --project --default gitdvccml  ../dvcRemoteLocal/gitdvccml
git add .dvc/config .dvc/.gitignore .dvcignore
git commit -m "New files after dvc init and dvc add remote"

# Add data
dvc add ./data/remote/creditcard.csv.gz
git add data/remote/creditcard2.csv.gz.dvc
git commit -m "Add data to dvc"
dvc push

# Run pipeline
dvc repro
git add dvc.lock
git commit -m "Updates after dvc repro"
dvc push

# List data files
dvc ls -R --dvc-only .
```

## Add new stage in Pipeline

- Add python script scale.py to stage scale
- Add stage to dvc.yaml
- Add params for stage to params.yaml


```
# Add changes
git add ./scripts/scale.py dvc.yaml params.yaml

# Run pipeline
dvc repro
# data/remote/creditcard.csv.gz.dvc didnt change, skipping           
# Stage ingest didnt change, skipping
# Stage clean didnt change, skipping
# Stage featurize didnt change, skipping
# Running stage scale
#  python scripts/scale/scale.py -i data/featurized/creditcard.csv
#  Updating lock file dvc.lock
# To track the changes with git, run:
#        git add dvc.lock
# To enable auto staging, run:
#         dvc config core.autostage true
# Use dvc push to send your updates to remote storage.

git add dvc.lock
git commit -m "Updates after adding stage scale and running dvc repro"
dvc push

# List data files
dvc ls -R --dvc-only .
```
