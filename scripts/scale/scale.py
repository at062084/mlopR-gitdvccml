#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
created on 2023-04-05

@author: at001335
"""
# from os import path
from os import access, R_OK
from os.path import isfile
import optparse
import logging
import shutil



# ----------------------------------------------------------------------------
# main
# ----------------------------------------------------------------------------

def main():

    usage = "usage: %prog [options] arguments"
    p = optparse.OptionParser()
    # Logging
    p.add_option('--logDir', '-L', default='log')
    p.add_option('--logFile', '-l', default='gitdvccml.log')
    # Environment
    p.add_option('--env', '-e', default='dev')
    # Directories
    p.add_option('--inFile', '-i', default="./data/featurized/creditcard.csv")
    p.add_option('--outFile', '-o', default="./data/scaled/creditcard.csv")

    # Retrieve parameters
    options, arguments = p.parse_args()

    # Remove any loggers for a clean start
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)

    # write to file and console
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s %(levelname)s %(funcName)s _%(lineno)d_ %(message)s',
                        datefmt='%Y-%m-%dT%H:%M:%S',
                        handlers=[logging.FileHandler(options.logDir + '/' + options.logFile),
                                  logging.StreamHandler()])

    # Variables for options
    env = options.env  
    inFile = options.inFile
    outFile = options.outFile

    # Give some info on invocation
    logging.info("Executing scale.py")
    logging.info('Running with options: ' +
                    ' inFile=' + str(inFile) +
                    ' outFile=' + str(outFile) +
                    '')
    
    logging.info("Copying " + inFile + " to " + outFile)
    shutil.copyfile(inFile, outFile)

    logging.info("Finished scale.py")

# ----------------------------------------------------------------------------
# invocation
# ----------------------------------------------------------------------------
if __name__ == "__main__":
    main()

